﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QuanLyDiemSinhVien
{
    public partial class xtraInBangDiemTong : DevExpress.XtraReports.UI.XtraReport
    {
        public xtraInBangDiemTong(string maLop)
        {
            InitializeComponent();
            ds1.EnforceConstraints = false;
            this.sp_BangDiemTongTableAdapter1.Fill(ds1.sp_BangDiemTong, maLop);
        }

    }
}
