﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace QuanLyDiemSinhVien
{
    public partial class frmChuyenLop : DevExpress.XtraEditors.XtraForm
    {
        public frmChuyenLop()
        {
            InitializeComponent();
        }
        public bool isComplete = false;
        private void FrmChuyenLop_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dS.LOP' table. You can move, or remove it, as needed.
            this.dS.EnforceConstraints = false;
            
            this.getListClassTableAdapter.Connection.ConnectionString = Program.connstr;
            
            this.getListClassTableAdapter.Fill(this.dS.GetListClass);
            // TODO: This line of code loads data into the 'dS.GetListClass' table. You can move, or remove it, as needed.
          
            cbKhoaChuyen.DataSource = Program.bds_dspm;
            cbKhoaChuyen.SelectedIndex = Program.mChinhanh;
            cbKhoaChuyen.DisplayMember = "TENKHOA";
            cbKhoaChuyen.ValueMember = "TENSERVER";
            isComplete = true;
          
        }

        private void CbKhoaChuyen_SelectedIndexChanged(object sender, EventArgs e)
        {
           
            if (isComplete)
            {
                Program.isClass = false;
                Program.servername = cbKhoaChuyen.SelectedValue.ToString();

                if (Program.mChinhanh == cbKhoaChuyen.SelectedIndex)
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }
                else
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối tới phân mảng mới", "Lỗi kết nối");
                    return;
                }
                this.dS.EnforceConstraints = false;

                this.getListClassTableAdapter.Connection.ConnectionString = Program.connstr;
                try
                {
                    this.getListClassTableAdapter.Fill(this.dS.GetListClass);

                }
                catch (Exception)
                {

                }

            }
        }

        private void BtnXacNhan_Click(object sender, EventArgs e)

        {
            if(txtMaLop.Text == txtMaLopCu.Text)
            {
                MessageBox.Show("Lớp cũ và lớp mới giống nhau");
                return;
            }
            string lop = ((cbLop.SelectedItem as DataRowView).Row as DS.GetListClassRow).TENLOP;
            DialogResult ret = MessageBox.Show("bạn có chắc muốn chuyển sinh viên: " + txtTen.Text + " từ lớp " + txtLopCu.Text + " sang lớp " + lop ,"Thông báo chuyển lớp",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning);
            if(ret == DialogResult.OK)
            {
                DialogResult = ret;
            }
        }
    }
}