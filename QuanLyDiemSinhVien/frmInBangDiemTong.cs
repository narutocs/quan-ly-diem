﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;

namespace QuanLyDiemSinhVien
{
    public partial class frmInBangDiemTong : DevExpress.XtraEditors.XtraForm
    {
        public frmInBangDiemTong()
        {
            InitializeComponent();
            Program.bds_dspm.Filter = "TENKHOA LIKE '%Khoa%'";

            cbKhoa.DataSource = Program.bds_dspm;
            cbKhoa.SelectedIndex = Program.mChinhanh;
            cbKhoa.DisplayMember = "TENKHOA";
            cbKhoa.ValueMember = "TENSERVER";
            if (Program.role == ROLE.PGV)
            {
                cbKhoa.Enabled = true;
            }
            isComplete = true;
        }
        public bool isComplete = false;
        private void CbKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isComplete)
            {
                Program.servername = cbKhoa.SelectedValue.ToString();

                if (Program.mChinhanh == cbKhoa.SelectedIndex)
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }
                else
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối tới phân mảng mới", "Lỗi kết nối");
                    return;
                }
                this.dS.EnforceConstraints = false;
                this.getListClassTableAdapter.Connection.ConnectionString = Program.connstr;

                this.getListClassTableAdapter.Fill(this.dS.GetListClass);
            }
        }

        private void FrmInBangDiemTong_Load(object sender, EventArgs e)
        {
            this.getListClassTableAdapter.Connection.ConnectionString = Program.connstr;
            // TODO: This line of code loads data into the 'dS.GetListClass' table. You can move, or remove it, as needed.
            this.getListClassTableAdapter.Fill(this.dS.GetListClass);

        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {

            string maLop = txtMaLop.Text;
                
            xtraInBangDiemTong rpt = new xtraInBangDiemTong(maLop);
            rpt.lbLop.Text = (((cbTenLop.SelectedItem as DataRowView).Row) as QuanLyDiemSinhVien.DS.GetListClassRow).TENLOP;
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowPreviewDialog();
        }
    }
}