﻿namespace QuanLyDiemSinhVien
{
    partial class frmInDanhSachThi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label mAMHLabel;
            System.Windows.Forms.Label label6;
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbLanThi = new System.Windows.Forms.ComboBox();
            this.dateNgayThi = new DevExpress.XtraEditors.DateEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.btnPreview = new System.Windows.Forms.Button();
            this.txtMaMonHoc = new DevExpress.XtraEditors.TextEdit();
            this.sp_GetListSubjectBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dS = new QuanLyDiemSinhVien.DS();
            this.label4 = new System.Windows.Forms.Label();
            this.cbMonHoc = new System.Windows.Forms.ComboBox();
            this.txtMaLop = new DevExpress.XtraEditors.TextEdit();
            this.getListClassBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cbKhoa = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbLop = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.getListClassTableAdapter = new QuanLyDiemSinhVien.DSTableAdapters.GetListClassTableAdapter();
            this.tableAdapterManager = new QuanLyDiemSinhVien.DSTableAdapters.TableAdapterManager();
            this.sp_GetListSubjectTableAdapter = new QuanLyDiemSinhVien.DSTableAdapters.sp_GetListSubjectTableAdapter();
            mAMHLabel = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateNgayThi.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNgayThi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaMonHoc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_GetListSubjectBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaLop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getListClassBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mAMHLabel
            // 
            mAMHLabel.AutoSize = true;
            mAMHLabel.Location = new System.Drawing.Point(266, 89);
            mAMHLabel.Name = "mAMHLabel";
            mAMHLabel.Size = new System.Drawing.Size(68, 13);
            mAMHLabel.TabIndex = 20;
            mAMHLabel.Text = "Mã môn học:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(289, 120);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(43, 13);
            label6.TabIndex = 25;
            label6.Text = "Lần thi:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbLanThi);
            this.panel1.Controls.Add(label6);
            this.panel1.Controls.Add(this.dateNgayThi);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.btnPreview);
            this.panel1.Controls.Add(mAMHLabel);
            this.panel1.Controls.Add(this.txtMaMonHoc);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.cbMonHoc);
            this.panel1.Controls.Add(this.txtMaLop);
            this.panel1.Controls.Add(this.cbKhoa);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbLop);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(549, 189);
            this.panel1.TabIndex = 0;
            // 
            // cbLanThi
            // 
            this.cbLanThi.FormattingEnabled = true;
            this.cbLanThi.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cbLanThi.Location = new System.Drawing.Point(348, 120);
            this.cbLanThi.Name = "cbLanThi";
            this.cbLanThi.Size = new System.Drawing.Size(113, 21);
            this.cbLanThi.TabIndex = 26;
            // 
            // dateNgayThi
            // 
            this.dateNgayThi.EditValue = new System.DateTime(2019, 12, 26, 22, 54, 33, 0);
            this.dateNgayThi.Location = new System.Drawing.Point(87, 117);
            this.dateNgayThi.Name = "dateNgayThi";
            this.dateNgayThi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateNgayThi.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateNgayThi.Size = new System.Drawing.Size(157, 20);
            this.dateNgayThi.TabIndex = 24;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Ngày thi:";
            // 
            // btnPreview
            // 
            this.btnPreview.Location = new System.Drawing.Point(236, 145);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(75, 23);
            this.btnPreview.TabIndex = 22;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.BtnPreview_Click);
            // 
            // txtMaMonHoc
            // 
            this.txtMaMonHoc.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.sp_GetListSubjectBindingSource, "MAMH", true));
            this.txtMaMonHoc.Location = new System.Drawing.Point(348, 85);
            this.txtMaMonHoc.Name = "txtMaMonHoc";
            this.txtMaMonHoc.Size = new System.Drawing.Size(113, 20);
            this.txtMaMonHoc.TabIndex = 21;
            // 
            // sp_GetListSubjectBindingSource
            // 
            this.sp_GetListSubjectBindingSource.DataMember = "sp_GetListSubject";
            this.sp_GetListSubjectBindingSource.DataSource = this.dS;
            // 
            // dS
            // 
            this.dS.DataSetName = "DS";
            this.dS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 88);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Tên môn học:";
            // 
            // cbMonHoc
            // 
            this.cbMonHoc.DataSource = this.sp_GetListSubjectBindingSource;
            this.cbMonHoc.DisplayMember = "TENMH";
            this.cbMonHoc.FormattingEnabled = true;
            this.cbMonHoc.Location = new System.Drawing.Point(87, 85);
            this.cbMonHoc.Name = "cbMonHoc";
            this.cbMonHoc.Size = new System.Drawing.Size(157, 21);
            this.cbMonHoc.TabIndex = 19;
            this.cbMonHoc.ValueMember = "MAMH";
            // 
            // txtMaLop
            // 
            this.txtMaLop.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.getListClassBindingSource, "MALOP", true));
            this.txtMaLop.Location = new System.Drawing.Point(348, 49);
            this.txtMaLop.Name = "txtMaLop";
            this.txtMaLop.Size = new System.Drawing.Size(113, 20);
            this.txtMaLop.TabIndex = 19;
            // 
            // getListClassBindingSource
            // 
            this.getListClassBindingSource.DataMember = "GetListClass";
            this.getListClassBindingSource.DataSource = this.dS;
            // 
            // cbKhoa
            // 
            this.cbKhoa.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKhoa.Enabled = false;
            this.cbKhoa.FormattingEnabled = true;
            this.cbKhoa.Location = new System.Drawing.Point(87, 12);
            this.cbKhoa.Name = "cbKhoa";
            this.cbKhoa.Size = new System.Drawing.Size(157, 21);
            this.cbKhoa.TabIndex = 18;
            this.cbKhoa.SelectedIndexChanged += new System.EventHandler(this.CbKhoa_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Chọn Khoa: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(290, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mã lớp:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(32, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tên lớp:";
            // 
            // cbLop
            // 
            this.cbLop.DataSource = this.getListClassBindingSource;
            this.cbLop.DisplayMember = "TENLOP";
            this.cbLop.FormattingEnabled = true;
            this.cbLop.Location = new System.Drawing.Point(87, 49);
            this.cbLop.Name = "cbLop";
            this.cbLop.Size = new System.Drawing.Size(157, 21);
            this.cbLop.TabIndex = 0;
            this.cbLop.ValueMember = "MALOP";
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 189);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(549, 123);
            this.panel2.TabIndex = 1;
            // 
            // getListClassTableAdapter
            // 
            this.getListClassTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.DIEMTableAdapter = null;
            this.tableAdapterManager.GIANGVIENTableAdapter = null;
            this.tableAdapterManager.KHOATableAdapter = null;
            this.tableAdapterManager.LOPTableAdapter = null;
            this.tableAdapterManager.MONHOCTableAdapter = null;
            this.tableAdapterManager.SINHVIENTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = QuanLyDiemSinhVien.DSTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // sp_GetListSubjectTableAdapter
            // 
            this.sp_GetListSubjectTableAdapter.ClearBeforeFill = true;
            // 
            // frmInDanhSachThi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(549, 312);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmInDanhSachThi";
            this.Text = "In danh sách thi";
            this.Load += new System.EventHandler(this.FrmInDanhSachThi_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateNgayThi.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateNgayThi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaMonHoc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_GetListSubjectBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaLop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getListClassBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private DS dS;
        private System.Windows.Forms.BindingSource getListClassBindingSource;
        private DSTableAdapters.GetListClassTableAdapter getListClassTableAdapter;
        private DSTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbLop;
        private DevExpress.XtraEditors.TextEdit txtMaLop;
        private System.Windows.Forms.ComboBox cbKhoa;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.BindingSource sp_GetListSubjectBindingSource;
        private DSTableAdapters.sp_GetListSubjectTableAdapter sp_GetListSubjectTableAdapter;
        private DevExpress.XtraEditors.TextEdit txtMaMonHoc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbMonHoc;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.ComboBox cbLanThi;
        private DevExpress.XtraEditors.DateEdit dateNgayThi;
        private System.Windows.Forms.Label label5;
    }
}