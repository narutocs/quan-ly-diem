﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using DevExpress.XtraGrid.Views.Grid;

namespace QuanLyDiemSinhVien
{
    public partial class frmTaoTaiKhoan : DevExpress.XtraEditors.XtraForm
    {
        public frmTaoTaiKhoan()
        {
            InitializeComponent();
            Program.bds_dspm.Filter = "TENKHOA LIKE '%Khoa%'";
            cbKhoa.DataSource = Program.bds_dspm;
            cbKhoa.SelectedIndex = Program.mChinhanh;
            cbKhoa.DisplayMember = "TENKHOA";
            cbKhoa.ValueMember = "TENSERVER";
            if(Program.role == ROLE.Khoa)
            {
                rbPGV.Enabled = false;
                rbPKT.Enabled = false;
                rbKhoa.Checked = true;
            }
            if(Program.role == ROLE.PKeToan)
            {
                rbKhoa.Enabled = false;
                rbPGV.Enabled = false;
                rbPKT.Checked = true;
            }
            isComplete = true;
        }
        public bool isComplete = false;
        private void CbKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isComplete)
            {
                Program.servername = cbKhoa.SelectedValue.ToString();

                if (Program.mChinhanh == cbKhoa.SelectedIndex)
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }
                else
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối tới phân mảng mới", "Lỗi kết nối");
                    return;
                }
                this.gIANGVIENTableAdapter.Connection.ConnectionString = Program.connstr;
                // TODO: This line of code loads data into the 'dS.GIANGVIEN' table. You can move, or remove it, as needed.
                this.gIANGVIENTableAdapter.Fill(this.dS.GIANGVIEN);
            }
        }

      

        private void FrmTaoTaiKhoan_Load(object sender, EventArgs e)
        {
            this.gIANGVIENTableAdapter.Connection.ConnectionString = Program.connstr;
            // TODO: This line of code loads data into the 'dS.GIANGVIEN' table. You can move, or remove it, as needed.
            this.gIANGVIENTableAdapter.Fill(this.dS.GIANGVIEN);

        }

        private void Panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void BtnXacNhan_Click(object sender, EventArgs e)
        {
            GridView gridView = grGiangVien.FocusedView as GridView;
            DataRowView data = gridView.GetRow(gridView.FocusedRowHandle) as DataRowView;
            DS.GIANGVIENRow row = data.Row as DS.GIANGVIENRow;
            string maGiangVien = row.MAGV;
            string loginName = txtLoginName.Text;
            string password = txtPassword.Text;
            string role = "";
            if (rbPKT.Checked)
            {
                role = "PKeToan";
                Program.servername = "NHUT-PC\\SRV3";
                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối tới phân mảng mới", "Lỗi kết nối");
                    return;
                }
            }
            else
            {
                role = rbKhoa.Checked ? "Khoa" : "PGV";
            }
            string query = "sp_CreateLogin";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@USERNAME",maGiangVien);
            command.Parameters.AddWithValue("@PASS",password);
            command.Parameters.AddWithValue("@LGNAME", loginName);
            command.Parameters.AddWithValue("@ROLE", role);
            var returnParameter = command.Parameters.Add("@ReturnVal", SqlDbType.Int);
            returnParameter.Direction = ParameterDirection.ReturnValue;

            command.ExecuteNonQuery();
            int result = int.Parse(returnParameter.Value.ToString());

            if(result == 0)
            {
                MessageBox.Show("Tạo tài khoản thành công");
            }
            else if(result < 3)
            {
                MessageBox.Show("Tài khoản đã tồn tại");
            }
            else
            {
                MessageBox.Show("Tạo tài khoản thất bại");
            }
        }
    }
}