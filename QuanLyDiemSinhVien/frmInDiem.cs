﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;

namespace QuanLyDiemSinhVien
{
    public partial class frmInDiem : DevExpress.XtraEditors.XtraForm
    {
        public frmInDiem()
        {
            InitializeComponent();
            if (Program.role == ROLE.PGV)
            {
                cbKhoa.Enabled = true;
            }
        }
        public bool isComplete = false;
        private void FrmInDiem_Load(object sender, EventArgs e)
        {
            this.sp_GetListSubjectTableAdapter.Connection.ConnectionString = Program.connstr;
            this.getListClassTableAdapter.Connection.ConnectionString = Program.connstr;

            this.sp_GetListSubjectTableAdapter.Fill(this.dS.sp_GetListSubject);
            this.getListClassTableAdapter.Fill(this.dS.GetListClass);

            Program.bds_dspm.Filter = "TENKHOA LIKE '%Khoa%'";

            cbKhoa.DataSource = Program.bds_dspm;
            cbKhoa.SelectedIndex = Program.mChinhanh;
            cbKhoa.DisplayMember = "TENKHOA";
            cbKhoa.ValueMember = "TENSERVER";
            cbLanThi.SelectedIndex = 0;

            isComplete = true;

        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            string maLop = txtMaLop.Text;
            string maMonHoc = txtMaMonHoc.Text;
            int lanThi = int.Parse(cbLanThi.SelectedItem.ToString());

            xtraInBangDiem rpt = new xtraInBangDiem(maLop,maMonHoc,lanThi);
            rpt.lbLop.Text = (((cbLop.SelectedItem as DataRowView).Row) as QuanLyDiemSinhVien.DS.GetListClassRow).TENLOP;
            rpt.lbMonHoc.Text = (((cbMonHoc.SelectedItem as DataRowView).Row) as QuanLyDiemSinhVien.DS.sp_GetListSubjectRow).TENMH;
            rpt.lbLanThi.Text = cbLanThi.SelectedItem.ToString();
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowPreviewDialog();
        }

        private void CbKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isComplete)
            {
                Program.servername = cbKhoa.SelectedValue.ToString();

                if (Program.mChinhanh == cbKhoa.SelectedIndex)
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }
                else
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối tới phân mảng mới", "Lỗi kết nối");
                    return;
                }
                this.dS.EnforceConstraints = false;
                txtMaLop.Text = cbKhoa.SelectedItem.ToString();
                this.getListClassTableAdapter.Connection.ConnectionString = Program.connstr;

                this.getListClassTableAdapter.Fill(this.dS.GetListClass);
            }
        }
    }
}