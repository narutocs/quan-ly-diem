﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyDiemSinhVien
{
    public class LogAction
    {
        public TYPE type { get; set; }
        public Dictionary<string, object> data { get; set; }
        public LogAction()
        {
            data = new Dictionary<string, object>();
        }
        public LogAction(TYPE type, Dictionary<string,object> data)
        {
            this.data = data;
            this.type = type;
        }
    }
}
