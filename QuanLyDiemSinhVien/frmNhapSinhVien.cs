﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyDiemSinhVien
{
    public partial class frmNhapSinhVien : DevExpress.XtraEditors.XtraForm
    {

        public frmNhapSinhVien(string maLop, string maSV)
        {
            InitializeComponent();
            txtLop.Text = maLop;
            txtMaSinhVien.Text = maSV;
        }

        private void BtnGhi_Click(object sender, EventArgs e)
        {
            bool isError = false;
            if (String.IsNullOrEmpty(txtDiaChi.Text))
            {
                MessageBox.Show("Địa chỉ là bắt buộc");
                isError = true;
            }
            if (String.IsNullOrEmpty(txten.Text))
            {
                MessageBox.Show("Tên là bắt buộc");
                isError = true;
            }
            if (String.IsNullOrEmpty(txtHo.Text))
            {
                MessageBox.Show("Họ là bắt buộc");
                isError = true;
            }
            if (String.IsNullOrEmpty(txtMaSinhVien.Text))
            {
                MessageBox.Show("Mã sinh viên là bắt buộc");
                isError = true;
            }
            if (String.IsNullOrEmpty(txtNgaySinh.Text))
            {
                MessageBox.Show("Ngày sinh là bắt buộc");
                isError = true;
            }
            if (String.IsNullOrEmpty(txtNoiSinh.Text))
            {
                MessageBox.Show("Nơi sinh là bắt buộc");
                isError = true;
            }
            if (!isError)
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void FrmNhapSinhVien_Load(object sender, EventArgs e)
        {
           

        }
    }
}