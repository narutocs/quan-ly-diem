﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QuanLyDiemSinhVien
{
    public partial class xtraInDanhSachDongHocPhi : DevExpress.XtraReports.UI.XtraReport
    {
        public xtraInDanhSachDongHocPhi(string maLop,string nienKhoa,int hocKy)
        {
            InitializeComponent();
            ds1.EnforceConstraints = false;
            this.sp_DanhSachHocPhiTheoLopTableAdapter.Fill(ds1.sp_DanhSachHocPhiTheoLop, maLop,nienKhoa,hocKy);
        }

    }
}
