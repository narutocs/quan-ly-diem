﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;

namespace QuanLyDiemSinhVien
{
    public partial class frmInDanhSachSinhVien : DevExpress.XtraEditors.XtraForm
    {
        public frmInDanhSachSinhVien()
        {
            InitializeComponent();
            if (Program.role == ROLE.PGV)
            {
                cbKhoa.Enabled = true;
            }
        }

        public bool isComplete = false;

        private void FrmInDanhSachSinhVien_Load(object sender, EventArgs e)
        {
            this.getListClassTableAdapter.Connection.ConnectionString = Program.connstr;
          
            Program.bds_dspm.Filter = "TENKHOA LIKE '%Khoa%'";

            cbKhoa.DataSource = Program.bds_dspm;
            cbKhoa.SelectedIndex = Program.mChinhanh;
            cbKhoa.DisplayMember = "TENKHOA";
            cbKhoa.ValueMember = "TENSERVER";



            this.getListClassTableAdapter.Fill(this.dS.GetListClass);



            isComplete = true;
        }

        private void CbKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isComplete)
            {
                Program.servername = cbKhoa.SelectedValue.ToString();

                if (Program.mChinhanh == cbKhoa.SelectedIndex)
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }
                else
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối tới phân mảng mới", "Lỗi kết nối");
                    return;
                }
                this.dS.EnforceConstraints = false;
                txtMaLop.Text = cbKhoa.SelectedItem.ToString();
                this.getListClassTableAdapter.Connection.ConnectionString = Program.connstr;

                this.getListClassTableAdapter.Fill(this.dS.GetListClass);
            }

        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            string maLop = txtMaLop.Text;
            xtraInDanhSachSinhVien rpt = new xtraInDanhSachSinhVien(maLop);
            rpt.lbLop.Text = (((cbLop.SelectedItem as DataRowView).Row) as QuanLyDiemSinhVien.DS.GetListClassRow).TENLOP;
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowPreviewDialog();

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
    }
}