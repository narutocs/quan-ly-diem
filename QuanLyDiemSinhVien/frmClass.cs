﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using DevExpress.XtraGrid.Views.Grid;

namespace QuanLyDiemSinhVien
{
    public partial class frmClass : DevExpress.XtraEditors.XtraForm
    {
        public frmClass()
        {
            InitializeComponent();
            if (Program.role == ROLE.PGV)
            {
                cbKhoa.Enabled = true;
            }
        }
        public int index = 0;
        public bool isComplete = false;
        public string maKhoa = "";
        public bool isAdd = false;
        public int position = 0;
        public string maLopTemp;
        public string tenLopTemp;
        public string maKhoaTemp;
        public Stack<LogAction> logData = new Stack<LogAction>();
        public Stack<LogAction> logDataStudent = new Stack<LogAction>();
        private void FrmClass_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dS.DIEM' table. You can move, or remove it, as needed.
         
            // TODO: This line of code loads data into the 'dS.DIEM' table. You can move, or remove it, as needed.

            this.dS.EnforceConstraints = false;
            
            this.lOPTableAdapter.Connection.ConnectionString = Program.connstr;
            this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connstr;
            this.dIEMTableAdapter.Fill(this.dS.DIEM);
            Program.bds_dspm.Filter = "TENKHOA LIKE '%Khoa%'";
           
            cbKhoa.DataSource = Program.bds_dspm;
            cbKhoa.SelectedIndex = Program.mChinhanh;
            cbKhoa.DisplayMember = "TENKHOA";
            cbKhoa.ValueMember = "TENSERVER";
            cbKhoa.SelectedIndex = -1;
            cbKhoa.SelectedIndex = 0;

            this.lOPTableAdapter.Fill(this.dS.LOP);
            this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
            maKhoa = txtMaKhoa.Text;
            isComplete = true;
            btnAdd.Enabled = false;
            btnUndo.Enabled = false;

           
        }

        public void enableTxt()
        {
            txtLop.Enabled = true;
            txtTenLop.Enabled = true;
            btnAdd.Enabled = true;
        }

        private void BtnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            position = BSLOP.Position;
            btnThem.Enabled = false;
            btnExit.Enabled = true;
            cbKhoa.Enabled = false;
            this.BSLOP.AddNew();
            enableTxt();
            btnEdit.Enabled = false;
            btnXoa.Enabled = false;
            txtLop.Focus();
            txtMaKhoa.Text = maKhoa;
            isAdd = true;
        }
        public int deleteData(String maLop)
        {
            String query = "EXEC dbo.sp_DeleteClass '" + maLop + "'";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.CommandType = CommandType.Text;
            int result = command.ExecuteNonQuery();
            BSLOP.RemoveCurrent();
            return result;
        }
        public int createData(String maLop,String tenLop,String maKhoa)
        {
            String query = "EXEC dbo.sp_InsertCLass @malop = '" + maLop + "', @tenlop = N'" + tenLop + "',@makh = '" + maKhoa + "' ";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.CommandType = CommandType.Text;
            int result = command.ExecuteNonQuery();
            return result;
        }
        public int updateData(String maLop,String tenLop,String maKhoa)
        {
            String query = "EXEC sp_UpdateClass @malop = '" + maLop + "', @tenlop = N'" + tenLop + "',@makh = '" + maKhoa + "' ";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.CommandType = CommandType.Text;
            int result = command.ExecuteNonQuery();
            return result;

        }

        private void BtnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(BSSV.Count > 0)
            {
                MessageBox.Show("Không thể xóa lớp vì có sinh viên trong lớp này");
                return;
            }
            DialogResult ret = MessageBox.Show("Bạn có chắc muốn xóa?","Cảnh báo xóa",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            if(ret == DialogResult.Yes)
            {
                try
                {
                    BSLOP.EndEdit();
                    btnUndo.Enabled = true;
                    Dictionary<string, object> dataDeleted = new Dictionary<string, object>();
                    dataDeleted.Add("maLop", txtLop.Text);
                    dataDeleted.Add("maKhoa", txtMaKhoa.Text);
                    dataDeleted.Add("tenLop", txtTenLop.Text);
                    logData.Push(new LogAction(TYPE.DELETE, dataDeleted)); // add data cũ
                    int result = deleteData(txtLop.Text);
                    if (result == -1)
                    {
                        MessageBox.Show("Xóa thất bại");
                        return;
                    }

                }
                catch(Exception ex)
                {
                   
                }
            }
        }

        private void BtnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            position = BSLOP.Position;
            btnExit.Enabled = true;
            cbKhoa.Enabled = false;
            txtLop.Enabled = false;
            txtTenLop.Enabled = true;
            btnAdd.Enabled = true;
            btnThem.Enabled = false;
            btnEdit.Enabled = false;
            btnXoa.Enabled = false;
            isAdd = false;
            maKhoaTemp = txtMaKhoa.Text;
            tenLopTemp = txtTenLop.Text;
            maLopTemp = txtLop.Text;
        }

        private void BtnAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            String maLop = txtLop.Text;
            if(maLop.Length < 7)
            {
                MessageBox.Show("Mã lớp phải từ 7 ký tự");
                return;
            }
            String tenLop = txtTenLop.Text;
            String maKhoa = txtMaKhoa.Text;
            if (isAdd)
            {
                if (String.IsNullOrEmpty(tenLop))
                {
                    MessageBox.Show( "Tên lớp không được để trống");
                    return;
                }
                if (String.IsNullOrEmpty(maLop))
                {
                    MessageBox.Show("Mã lớp không được để trống");
                    return;
                }
                int result = createData(maLop, tenLop, maKhoa);
                if (result == -1)
                {
                    MessageBox.Show("Mã khoa hoặc tên lớp đã tồn tại");
                    return;
                }
                else
                {
                    btnUndo.Enabled = true;
                    Dictionary<string, object> dataDeleted = new Dictionary<string, object>();
                    dataDeleted.Add("maLop", txtLop.Text);
                    dataDeleted.Add("maKhoa", txtMaKhoa.Text);
                    dataDeleted.Add("tenLop", txtTenLop.Text);
                    logData.Push(new LogAction(TYPE.ADD, dataDeleted)); // add data cũ
                    MessageBox.Show("Thêm lớp thành công");
                    BSLOP.EndEdit();
                    BSLOP.ResetCurrentItem();
                }
            }
            else
            {
                btnUndo.Enabled = true;
                int result = updateData(maLop, tenLop, maKhoa);
                if (result == -1)
                {
                    MessageBox.Show("Tên lớp đã tồn tại");
                }
                else
                {
                    Dictionary<string, object> dataDeleted = new Dictionary<string, object>();
                    dataDeleted.Add("maLop", maLopTemp);
                    dataDeleted.Add("maKhoa", maKhoaTemp);
                    dataDeleted.Add("tenLop", tenLopTemp);
                    logData.Push(new LogAction(TYPE.UPDATE, dataDeleted)); // add data cũ
                    MessageBox.Show("Update lớp thành công");
                    BSLOP.EndEdit();
                    BSLOP.ResetCurrentItem();
                }
               
            }
            btnExit.Enabled = false;
            grLop.Enabled = true;
            grSV.Enabled = true;
            cbKhoa.Enabled = true;
            txtLop.Enabled = false;
            txtTenLop.Enabled = false;
            btnThem.Enabled = true;
            btnAdd.Enabled = false;
            btnXoa.Enabled = true;
            btnEdit.Enabled = true;
        }

        private void BtnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(logData.Count > 0)
            {
                LogAction action = logData.Pop();
                if(logData.Count == 0)
                {
                    btnUndo.Enabled = false;
                }
                Dictionary<string,object> row = action.data;
                string maLop = row["maLop"].ToString();
                string tenLop = row["tenLop"].ToString();
                string maKhoa = row["maKhoa"].ToString();
                switch (action.type)
                {
                    case TYPE.ADD:
                        deleteData(maLop);
                        break;
                    case TYPE.DELETE:
                        createData(maLop, tenLop, maKhoa);
                        BSLOP.EndEdit();
                        this.lOPTableAdapter.Fill(this.dS.LOP);
                        break;
                    case TYPE.UPDATE:
                        updateData(maLop, tenLop, maKhoa);
                        BSLOP.EndEdit();
                        this.lOPTableAdapter.Fill(this.dS.LOP);
                        break;
                }
            }
        }

        private void CbKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (isComplete && Program.isClass)
            {
                Program.servername = cbKhoa.SelectedValue.ToString();
               
                if (Program.mChinhanh == cbKhoa.SelectedIndex)
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }
                else
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối tới phân mảng mới", "Lỗi kết nối");
                    return;
                }
                this.dS.EnforceConstraints = false;

                this.lOPTableAdapter.Connection.ConnectionString = Program.connstr;
                this.sINHVIENTableAdapter.Connection.ConnectionString = Program.connstr;
                try
                {
                    this.lOPTableAdapter.Fill(this.dS.LOP);
                    
                    this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                    maKhoa = txtMaKhoa.Text;
                }
                catch (Exception)
                {

                }
               
            }
            

        }

        private void BtnRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.lOPTableAdapter.Fill(this.dS.LOP);
            BSLOP.Position = position;
        }

        private void BtnThemSV_Click(object sender, EventArgs e)
        {
            string maLop = txtLop.Text;
            string maSVTemp = getIdSinhVien(maLop);
            frmNhapSinhVien frm = new frmNhapSinhVien(maLop,maSVTemp);
            frm.pnNghiHoc.Enabled = false;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                string maSinhVien = frm.txtMaSinhVien.Text;
                string hoSinhVien = frm.txtHo.Text;
                string tenSinhVien = frm.txten.Text;
                string ngaySinh = frm.txtNgaySinh.Text;
                string diaChi = frm.txtDiaChi.Text;
                bool gioiTinh = frm.rbNam.Checked ;
                string ghiChu = frm.txtGhiChu.Text;
                string noiSinh = frm.txtNoiSinh.Text;
                int result = insertStudent(maLop, maSinhVien, hoSinhVien, tenSinhVien, ngaySinh, diaChi, gioiTinh, ghiChu,noiSinh);
                if(result == -1)
                {
                    MessageBox.Show("Thêm mới thất bại");
                    return;
                }
                else
                {
                    Dictionary<string, object> dataAction = new Dictionary<string, object>();
                    dataAction.Add("ho", hoSinhVien);
                    dataAction.Add("ten", tenSinhVien);
                    dataAction.Add("diachi", diaChi);
                    dataAction.Add("phai", gioiTinh );
                    dataAction.Add("noisinh", noiSinh);
                    dataAction.Add("masv", maSinhVien);
                    dataAction.Add("malop", maLop);
                    dataAction.Add("ngaysinh", ngaySinh);
                    dataAction.Add("nghihoc", false) ;
                    dataAction.Add("ghichu", ghiChu);
                    logDataStudent.Push(new LogAction(TYPE.ADD, dataAction)); // add data cũ

                    btnUndoSV.Enabled = true;
                    this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                    GridView gridView = grSV.FocusedView as GridView;
                    DataRowView data = gridView.GetRow(gridView.FocusedRowHandle) as DataRowView;
                    DS.SINHVIENRow row = data.Row as DS.SINHVIENRow;
                    
                }

            }
        }

        public int insertStudent(string maLop, string maSinhVien, string hoSinhVien, string tenSinhVien,string ngaySinh, string diaChi, bool gioiTinh, string ghiChu, string noiSinh)
        {
            string query = "EXEC sp_InsertStudent @maSV,@ho,@ten,@maLop,@phai,@ngaySinh,@noiSinh,@diaChi,@ghiChu,@nghiHoc";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.Parameters.AddWithValue("@maSV", maSinhVien);
            command.Parameters.AddWithValue("@ho", hoSinhVien);
            command.Parameters.AddWithValue("@ten", tenSinhVien);
            command.Parameters.AddWithValue("@maLop", maLop);
            command.Parameters.AddWithValue("@phai", gioiTinh);
            
            command.Parameters.AddWithValue("@ngaySinh", ngaySinh);
            command.Parameters.AddWithValue("@noiSinh", noiSinh);
            command.Parameters.AddWithValue("@diaChi", diaChi);
            command.Parameters.AddWithValue("@ghiChu", ghiChu);
            command.Parameters.AddWithValue("@nghiHoc", 0);
            return command.ExecuteNonQuery();
        }

        public int editStudent( string maSinhVien, string hoSinhVien, string tenSinhVien, string ngaySinh, string diaChi, bool gioiTinh, string ghiChu, bool nghiHoc,string noiSinh)
        {
            string query = "EXEC sp_UpdateStudent @maSV,@ho,@ten,@phai,@ngaySinh,@noiSinh,@diaChi,@ghiChu,@nghiHoc";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.Parameters.AddWithValue("@maSV", maSinhVien);
            command.Parameters.AddWithValue("@ho", hoSinhVien);
            command.Parameters.AddWithValue("@ten", tenSinhVien);
            command.Parameters.AddWithValue("@phai", gioiTinh);
            DateTime date = Convert.ToDateTime(ngaySinh);

            command.Parameters.AddWithValue("@ngaySinh", date);
            command.Parameters.AddWithValue("@noiSinh", noiSinh);
            command.Parameters.AddWithValue("@diaChi", diaChi);
            command.Parameters.AddWithValue("@ghiChu", ghiChu);
            command.Parameters.AddWithValue("@nghiHoc", nghiHoc);

            return command.ExecuteNonQuery();
        }
        public string getIdSinhVien(string maLop)
        {
            SqlCommand commandGetID = new SqlCommand("sp_GetIdSinhVien", Program.conn);
            commandGetID.CommandType = CommandType.StoredProcedure;
            commandGetID.Parameters.AddWithValue("@maLop", maLop);
            commandGetID.Parameters.AddWithValue("@maSV", SqlDbType.VarChar).Direction = ParameterDirection.Output;
            SqlDataReader reader = commandGetID.ExecuteReader();
            reader.Read();
            string maSVTemp = reader.GetString(0);

            return maSVTemp;
        }
        private void BtnEditSV_Click(object sender, EventArgs e)
        {
            
            GridView gridView = grSV.FocusedView as GridView;
            DataRowView data = gridView.GetRow(gridView.FocusedRowHandle) as DataRowView;
            if(data == null)
            {
                MessageBox.Show("Chưa có sinh viên nào được chọn");
                return;
            }
            DS.SINHVIENRow row = data.Row as DS.SINHVIENRow;
            string maLop = txtLop.Text;
            string maSVTemp = row.MASV;
            frmNhapSinhVien frm = new frmNhapSinhVien(maLop,maSVTemp);
            frm.txtMaSinhVien.Text = row.MASV;
            frm.pnNghiHoc.Enabled = true;
            frm.txtMaSinhVien.Enabled = false;
            frm.txtHo.Text = row.HO;
            frm.txten.Text = row.TEN;
            frm.txtNoiSinh.Text = row.NOISINH;
            frm.txtNgaySinh.Text = row.NGAYSINH.ToString().Split(' ')[0];
            frm.txtDiaChi.Text = row.DIACHI;

            if (row.NGHIHOC)
            {
                frm.rbTrue.Checked = true;
            }
            else
            {
                frm.rbFalse.Checked = true;
            }
            if (row.PHAI)
            {
                frm.rbNam.Checked = true;
            }
            else
            {
                frm.rbNu.Checked = true;
            }
            frm.txtGhiChu.Text = row.GHICHU;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                string maSinhVien = frm.txtMaSinhVien.Text;
                string hoSinhVien = frm.txtHo.Text;
                string tenSinhVien = frm.txten.Text;
                string ngaySinh = frm.txtNgaySinh.Text;
                string diaChi = frm.txtDiaChi.Text;
                bool gioiTinh = frm.rbNam.Checked;
                string ghiChu = frm.txtGhiChu.Text;
                bool nghiHoc = frm.rbTrue.Checked;
                string noiSinh = frm.txtNoiSinh.Text;

                Dictionary<string, object> dataAction = new Dictionary<string, object>();
                dataAction.Add("ho", row.HO);
                dataAction.Add("ten", row.TEN);
                dataAction.Add("diachi", row.DIACHI);
                dataAction.Add("phai", row.PHAI);
                dataAction.Add("noisinh", row.NOISINH);
                dataAction.Add("masv", row.MASV);
                dataAction.Add("malop", row.MALOP);
                dataAction.Add("ngaysinh", row.NGAYSINH);
                dataAction.Add("nghihoc", row.NGHIHOC);
                dataAction.Add("ghichu", row.GHICHU);
                logDataStudent.Push(new LogAction(TYPE.UPDATE, dataAction)); // add data cũ

                int result = editStudent(maSinhVien, hoSinhVien, tenSinhVien, ngaySinh, diaChi, gioiTinh, ghiChu, nghiHoc,noiSinh);
                if (result == -1)
                {
                    MessageBox.Show("Thêm mới thất bại");
                    logDataStudent.Pop();
                    return;
                }
                else
                {
                    btnUndoSV.Enabled = true;
                    this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                }

            }
        }

        public int deleteStudent(string maSV)
        { 
            string query = "EXEC sp_DeleteStudent @maSV";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.Parameters.AddWithValue("@maSV", maSV);
            return command.ExecuteNonQuery();
        }


        public void AddActionStudent(DS.SINHVIENRow row,TYPE type)
        {
            Dictionary<string, object> dataAction = new Dictionary<string, object>();
            dataAction.Add("ho", row.HO);
            dataAction.Add("ten", row.TEN);
            dataAction.Add("diachi", row.DIACHI);
            dataAction.Add("phai", row.PHAI);
            dataAction.Add("noisinh", row.NOISINH);
            dataAction.Add("masv", row.MASV);
            dataAction.Add("malop", row.MALOP);
            dataAction.Add("ngaysinh", row.NGAYSINH);
            dataAction.Add("nghihoc", row.NGHIHOC);
            dataAction.Add("ghichu", row.GHICHU);
            logDataStudent.Push(new LogAction(type, dataAction)); // add data cũ
        }
        private void BtnDeleteSV_Click(object sender, EventArgs e)
        {
           

            GridView gridView = grSV.FocusedView as GridView;
            DataRowView data = gridView.GetRow(gridView.FocusedRowHandle) as DataRowView;

            if(data == null)
            {
                MessageBox.Show("Chưa có sinh viên nào được chọn");
                return;
            }
            DialogResult ret = MessageBox.Show("Bạn có chắc muốn xóa?", "Cảnh báo xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (ret == DialogResult.No)
            {
                return;
            }
            DS.SINHVIENRow row = data.Row as DS.SINHVIENRow;

            AddActionStudent(row,TYPE.DELETE);

            string maSV = row.MASV;
            int result = deleteStudent(maSV);
            if (result == -1)
            {
                MessageBox.Show("Không thể xóa sinh viên này");
                return;
            }
            else
            {
                BSSV.EndEdit();
                this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                btnUndoSV.Enabled = true;
            }
        }

        private void BtnUndoSV_Click(object sender, EventArgs e)
        {

            if (logDataStudent.Count > 0)
            {
                LogAction action = logDataStudent.Pop();
                if (logDataStudent.Count == 0)
                {
                    btnUndoSV.Enabled = false;
                }
                Dictionary<string, object> row = action.data;

                string maLop = row["malop"].ToString();
                string maSinhVien = row["masv"].ToString();
                string hoSinhVien = row["ho"].ToString();
                string tenSinhVien = row["ten"].ToString();
                string ngaySinh = row["ngaysinh"].ToString();
                string diaChi = row["diachi"].ToString();
                bool gioiTinh = row["phai"].ToString() == "true" ? true : false;
                string ghiChu = row["ghichu"].ToString();
                bool nghiHoc = row["nghihoc"].ToString() == "true" ? true:false;
                string noiSinh = row["noisinh"].ToString();
                switch (action.type)
                {
                    case TYPE.ADD:
                        deleteStudent(maSinhVien);
                        this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                        break;
                    case TYPE.DELETE:
                        insertStudent(maLop, maSinhVien, hoSinhVien, tenSinhVien, ngaySinh, diaChi, gioiTinh, ghiChu, noiSinh);
                        BSSV.EndEdit();
                        this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                        break;
                    case TYPE.UPDATE:
                        editStudent(maSinhVien, hoSinhVien, tenSinhVien, ngaySinh, diaChi, gioiTinh, ghiChu, nghiHoc, noiSinh);
                        BSSV.EndEdit();
                        this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                        break;
                }
            }
        }

        private void BtnRefreshSV_Click(object sender, EventArgs e)
        {
            this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
        }

        private void BtnChuyenLop_Click(object sender, EventArgs e)
        {
            frmChuyenLop frm = new frmChuyenLop();
            DS.LOPRow lop = (BSLOP.Current as DataRowView).Row as DS.LOPRow;
            DS.SINHVIENRow sv = (BSSV.Current as DataRowView).Row as DS.SINHVIENRow;
            if (sv.NGHIHOC)
            {
                MessageBox.Show("Sinh viên " + sv.HO + " " + sv.TEN + " đã nghỉ học vui lòng kiểm tra lại");
                return;
            }
            frm.txtLopCu.Text = lop.TENLOP;
            frm.txtMaLopCu.Text = lop.MALOP;
            frm.txtTen.Text = sv.HO + " " + sv.TEN;
            int chiNhanh = cbKhoa.SelectedIndex;
            if(frm.ShowDialog() == DialogResult.OK)
            {
                
                string maSV = sv.MASV;
                string maLop = frm.txtMaLop.Text;
                cbKhoa.SelectedIndex = chiNhanh;
                string query = "EXEC sp_ChuyenLop @maSV = '" + maSV.Trim()+"', @maLop ='" + maLop.Trim()+"'";
                SqlCommand command = new SqlCommand(query, Program.conn);

                int result = command.ExecuteNonQuery();
                if(result == -1)
                {
                    MessageBox.Show("Chuyển lớp thất bại");
                }
                else
                {
                    this.lOPTableAdapter.Fill(this.dS.LOP);
                    this.sINHVIENTableAdapter.Fill(this.dS.SINHVIEN);
                    MessageBox.Show("Chuyển lớp thành công");
                    
                }
            }
            
        }

        private void CbKhoa_Click(object sender, EventArgs e)
        {
            Program.isClass = true;
        }

        private void BtnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            btnThem.Enabled = true;
            txtTenLop.Enabled = false;
            cbKhoa.Enabled = true;
            txtLop.Enabled = false;
            btnAdd.Enabled = false;
            btnXoa.Enabled = true;
            btnEdit.Enabled = true;
            grSV.Enabled = true;
            grLop.Enabled = true;

            this.lOPTableAdapter.Fill(this.dS.LOP);
            BSLOP.Position = position;
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void GrLop_Click(object sender, EventArgs e)
        {

        }
    }
}