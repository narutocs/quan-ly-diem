﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QuanLyDiemSinhVien
{
    public partial class xtraInDanhSachThi : DevExpress.XtraReports.UI.XtraReport
    {
        public xtraInDanhSachThi(string maLop, int lanThi)
        {
            InitializeComponent();
            ds1.EnforceConstraints = false;
            this.sp_DanhSachThiHetMonTableAdapter1.Fill(ds1.sp_DanhSachThiHetMon, maLop, lanThi);
        }

    }
}
