﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace QuanLyDiemSinhVien
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmMain()
        {
            InitializeComponent();
            if (Program.role == ROLE.Khoa || Program.role == ROLE.PGV)
            {
                btnHocPhi.Enabled = false;
                btnDanhSachDongHocPhi.Enabled = false;
            }
            if(Program.role == ROLE.PKeToan)
            {
                btnInDanhSachSinhVien.Enabled = btnBangDiemMonHoc.Enabled = btnBangDiemTongKet.Enabled = btnDanhSachThiHetMon.Enabled  = btnPhieuDiem.Enabled = false;
                btnDiem.Enabled = btnLop.Enabled = btnMon.Enabled = false;
            }
        }

        private Form CheckExists(Type ftype)
        {
            foreach (Form f in this.MdiChildren)
                if (f.GetType() == ftype)
                    return f;
            return null;
        }



        private void BtnDangXuat_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void BtnThoat_ItemClick(object sender, ItemClickEventArgs e)
        {
            Application.Exit();
        }

        private void BtnLop_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmClass));
            if (frm != null) frm.Activate();
            else
            {
                frmClass f = new frmClass();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }


        private void BtnDiem_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmNhapDiem));
            if (frm != null) frm.Activate();
            else
            {
                frmNhapDiem f = new frmNhapDiem();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }

        private void BtnHocPhi_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmHocPhi));
            if (frm != null) frm.Activate();
            else
            {
                frmHocPhi f = new frmHocPhi();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }

        private void BtnMon_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmMonHoc));
            if (frm != null) frm.Activate();
            else
            {
                frmMonHoc f = new frmMonHoc();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }

        private void BtnInDanhSachSinhVien_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmInDanhSachSinhVien));
            if (frm != null) frm.Activate();
            else
            {
                frmInDanhSachSinhVien f = new frmInDanhSachSinhVien();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }

        private void BtnDanhSachThiHetMon_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmInDanhSachThi));
            if (frm != null) frm.Activate();
            else
            {
                frmInDanhSachThi f = new frmInDanhSachThi();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }

        private void BtnBangDiemMonHoc_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmInDiem));
            if (frm != null) frm.Activate();
            else
            {
                frmInDiem f = new frmInDiem();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }

        private void BtnPhieuDiem_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmBangDiemSinhVien));
            if (frm != null) frm.Activate();
            else
            {
                frmBangDiemSinhVien f = new frmBangDiemSinhVien();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }

        private void BtnDanhSachDongHocPhi_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmHocPhiTheoLop));
            if (frm != null) frm.Activate();
            else
            {
                frmHocPhiTheoLop f = new frmHocPhiTheoLop();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }

        private void BtnBangDiemTongKet_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmInBangDiemTong));
            if (frm != null) frm.Activate();
            else
            {
                frmInBangDiemTong f = new frmInBangDiemTong();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }

        private void BtnTaoLogin_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = this.CheckExists(typeof(frmTaoTaiKhoan));
            if (frm != null) frm.Activate();
            else
            {
                frmTaoTaiKhoan f = new frmTaoTaiKhoan();
                f.MdiParent = this;
                f.Show();
                Program.isClass = true;
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {

        }
    }
}