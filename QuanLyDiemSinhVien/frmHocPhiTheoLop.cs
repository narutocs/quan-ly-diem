﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;

namespace QuanLyDiemSinhVien
{
    public partial class frmHocPhiTheoLop : DevExpress.XtraEditors.XtraForm
    {
        public frmHocPhiTheoLop()
        {
            InitializeComponent();
        }
      

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            string maLop = txtMaLop.Text;
            string nienKhoa = cbNienKhoa.Text;
            int hocKy = int.Parse( cbHocKy.Text);
            
            xtraInDanhSachDongHocPhi rpt = new xtraInDanhSachDongHocPhi(maLop, nienKhoa,hocKy);
            rpt.lbTenLop.Text = (((cbLop.SelectedItem as DataRowView).Row) as QuanLyDiemSinhVien.DS.sp_getAllClassRow).TENLOP;
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowPreviewDialog();
        }

        private void FrmHocPhiTheoLop_Load(object sender, EventArgs e)
        {
            this.sp_getAllClassTableAdapter.Connection.ConnectionString = Program.connstr;
            // TODO: This line of code loads data into the 'dS.sp_getAllClass' table. You can move, or remove it, as needed.
            this.sp_getAllClassTableAdapter.Fill(this.dS.sp_getAllClass);

            int currentYear = DateTime.Now.Year;
            int index = -4;
            string[] tempYear = new string[11];
            for (int i = 0; i <= 10; i++)
            {
                tempYear[i] = (currentYear + index) + " - " + (currentYear + index + 1);
                index++;
            }
            cbNienKhoa.Items.AddRange(tempYear);
            cbNienKhoa.SelectedIndex = 4;
            cbHocKy.SelectedIndex = 0;


        }

        private void Panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}