﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QuanLyDiemSinhVien
{
    public partial class xtraInDanhSachSinhVien : DevExpress.XtraReports.UI.XtraReport
    {
        public xtraInDanhSachSinhVien(string maLop)
        {
            InitializeComponent();
            ds1.EnforceConstraints = false;
            this.sp_GetListStudentTableAdapter.Fill(ds1.sp_GetListStudent, maLop);
        }

    }

}
