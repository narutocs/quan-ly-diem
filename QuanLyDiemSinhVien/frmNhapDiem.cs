﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using DevExpress.XtraGrid.Views.Grid;

namespace QuanLyDiemSinhVien
{
    public partial class frmNhapDiem : DevExpress.XtraEditors.XtraForm
    {
        public frmNhapDiem()
        {
            InitializeComponent();
            if (Program.role == ROLE.PGV)
            {
                cbKhoa.Enabled = true;
            }
        }
        public bool isComplete = false;
        public bool isChange = true;
        public BindingSource binding;
        public bool flat = false;
        private void FrmNhapDiem_Load(object sender, EventArgs e)
        {
            this.getListClassTableAdapter.Connection.ConnectionString = Program.connstr;
            this.sp_GetListSubjectTableAdapter.Connection.ConnectionString = Program.connstr;
            // TODO: This line of code loads data into the 'dS.GetListClass' table. You can move, or remove it, as needed.
            this.getListClassTableAdapter.Fill(this.dS.GetListClass);
            // TODO: This line of code loads data into the 'dS.sp_GetListSubject' table. You can move, or remove it, as needed.
            this.sp_GetListSubjectTableAdapter.Fill(this.dS.sp_GetListSubject);

            cbKhoa.DataSource = Program.bds_dspm;
            cbKhoa.SelectedIndex = Program.mChinhanh;
            cbKhoa.DisplayMember = "TENKHOA";
            cbKhoa.ValueMember = "TENSERVER";
            cbLanThi.SelectedIndex = 0;
            isComplete = true;

        }
        

        private void BtnBatDau_Click(object sender, EventArgs e)
        {
            string query = "sp_GetInfoInsertMark";
            SqlCommand command = new SqlCommand(query,Program.conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@maMonHoc", txtMaMonHoc.Text);
            command.Parameters.AddWithValue("@maLop", txtMaLopHoc.Text);
            command.Parameters.AddWithValue("@lanThi", cbLanThi.SelectedItem.ToString());
            var returnParameter = command.Parameters.Add("@ReturnVal", SqlDbType.Int);
            returnParameter.Direction = ParameterDirection.ReturnValue;
            command.ExecuteNonQuery();
            int result = int.Parse(returnParameter.Value + "");

            if(result == 1)
            {
                gridColumn3.OptionsColumn.AllowEdit = false;
            }
            else
            {
                gridColumn3.OptionsColumn.AllowEdit = true;
            }

            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);
            binding = new BindingSource();
            binding.DataSource = table;
            grNhapDiem.DataSource = binding;
        }

        private void CbKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isComplete)
            {
                Program.servername = cbKhoa.SelectedValue.ToString();

                if (Program.mChinhanh == cbKhoa.SelectedIndex)
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }
                else
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối tới phân mảng mới", "Lỗi kết nối");
                    return;
                }


                this.getListClassTableAdapter.Connection.ConnectionString = Program.connstr;
                this.sp_GetListSubjectTableAdapter.Connection.ConnectionString = Program.connstr;
                this.getListClassTableAdapter.Fill(this.dS.GetListClass);
                    // TODO: This line of code loads data into the 'dS.sp_GetListSubject' table. You can move, or remove it, as needed.
                this.sp_GetListSubjectTableAdapter.Fill(this.dS.sp_GetListSubject);
            }
        }

        private void BtnGhi_Click(object sender, EventArgs e)
        {
            for(int i = 0; i < gridView1.DataRowCount; i++)
            {
                string diem = gridView1.GetRowCellValue(i, "DIEM").ToString().Trim();
                string maSV = gridView1.GetRowCellValue(i, "MASV").ToString().Trim();
                string lanThi = cbLanThi.SelectedItem.ToString().Trim();
                
                if (String.IsNullOrEmpty(diem))
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ điểm cho tất cả các sinh viên");
                    return;
                }
                else
                {
                    string query = "sp_InsertMark";
                    SqlCommand command = new SqlCommand(query, Program.conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@maMonHoc",txtMaMonHoc.Text);
                    command.Parameters.AddWithValue ("@maSV",maSV );
                    command.Parameters.AddWithValue("@lanThi", lanThi);
                    command.Parameters.AddWithValue("@diem", diem);

                    command.ExecuteNonQuery();
                }
            }
            MessageBox.Show("Cập nhật điểm cho sinh viên thành công");
        }

       

        private void GridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (isChange)
            {
                GridView gridView = grNhapDiem.FocusedView as GridView;
                DataRowView data = gridView.GetRow(gridView.FocusedRowHandle) as DataRowView;
                object temp = (data.Row.ItemArray)[3];
                if (float.TryParse(temp.ToString(), out float diem))
                {
                    if (diem < 0 || diem > 10)
                    {
                        MessageBox.Show("Điểm nhập không hợp lệ");
                        isChange = false;
                        gridView1.SetFocusedValue("");                       
                    }
                   
                }
                else
                {
                    MessageBox.Show("Điểm nhập không hợp lệ");
                    isChange = false;
                    gridView1.SetFocusedValue("");
                }
            }
            else
            {
                isChange = true;
            }
        }

        private void CbMonHoc_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}