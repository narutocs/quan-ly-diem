﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.UI;
using DevExpress.XtraGrid.Views.Grid;

namespace QuanLyDiemSinhVien
{
    public partial class frmBangDiemSinhVien : DevExpress.XtraEditors.XtraForm
    {
        public frmBangDiemSinhVien()
        {
            InitializeComponent();
            if(Program.role == ROLE.PGV)
            {
                cbKhoa.Enabled = true;
            }
        }
        public bool isComplete = false;
        private void FrmBangDiemSinhVien_Load(object sender, EventArgs e)
        {

            this.sp_getAllStudentTableAdapter.Connection.ConnectionString = Program.connstr;
            // TODO: This line of code loads data into the 'dS.sp_getAllStudent' table. You can move, or remove it, as needed.
            this.sp_getAllStudentTableAdapter.Fill(this.dS.sp_getAllStudent);


            Program.bds_dspm.Filter = "TENKHOA LIKE '%Khoa%'";

            cbKhoa.DataSource = Program.bds_dspm;
            cbKhoa.SelectedIndex = Program.mChinhanh;
            cbKhoa.DisplayMember = "TENKHOA";
            cbKhoa.ValueMember = "TENSERVER";

            isComplete = true;

        }

        private void BtnPreview_Click(object sender, EventArgs e)
        {
            GridView gridView = grSinhVien.FocusedView as GridView;
            DataRowView data = gridView.GetRow(gridView.FocusedRowHandle) as DataRowView;
            DS.sp_getAllStudentRow row = data.Row as DS.sp_getAllStudentRow;
            string maSinhVien = row.MASV;

            xtraInBangDiemTheoSinhVien rpt = new xtraInBangDiemTheoSinhVien(maSinhVien);
            rpt.lbLop.Text = row.MALOP;
            rpt.lbMaSinhVien.Text = row.MASV;
            rpt.lbTenSinhVien.Text = row.HO_TEN;
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowPreviewDialog();
        }

        private void CbKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (isComplete)
            {
                Program.servername = cbKhoa.SelectedValue.ToString();

                if (Program.mChinhanh == cbKhoa.SelectedIndex)
                {
                    Program.mlogin = Program.mloginDN;
                    Program.password = Program.passwordDN;
                }
                else
                {
                    Program.mlogin = Program.remotelogin;
                    Program.password = Program.remotepassword;
                }
                if (Program.KetNoi() == 0)
                {
                    XtraMessageBox.Show("Lỗi kết nối tới phân mảng mới", "Lỗi kết nối");
                    return;
                }
                this.dS.EnforceConstraints = false;
                this.sp_getAllStudentTableAdapter.Connection.ConnectionString = Program.connstr;

                this.sp_getAllStudentTableAdapter.Fill(this.dS.sp_getAllStudent);
            }
        }
    }
}