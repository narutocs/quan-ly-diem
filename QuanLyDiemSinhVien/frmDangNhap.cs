﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QuanLyDiemSinhVien
{
    public partial class frmDangNhap : DevExpress.XtraEditors.XtraForm
    {
        public frmDangNhap()
        {
            InitializeComponent();
        }

        private void FrmDangNhap_Load(object sender, EventArgs e)
        {
   
            this.v_DS_PHANMANHTableAdapter.Fill(this.dSPM.V_DS_PHANMANH);

            Program.bds_dspm = PM;
            cbKhoa.DisplayMember = "TENKHOA";
            cbKhoa.ValueMember = "TENSERVER";
            cbKhoa.SelectedIndex = -1;
            cbKhoa.SelectedIndex = 0;

         
        }

        private void BtnDangNhap_Click(object sender, EventArgs e)
        {
            if(cbKhoa.SelectedIndex == -1)
            {
                XtraMessageBox.Show("Bạn chưa chọn khoa", "Nhập sai");
                return;
            }
            if (string.IsNullOrEmpty(txtUsername.Text.Trim()))
            {
                XtraMessageBox.Show("username không được bỏ trống", "Nhập sai");
                return;
            }
            if (string.IsNullOrEmpty(txtPassword.Text.Trim()))
            {
                XtraMessageBox.Show("password không được bỏ trống", "Nhập sai");
                return;
            }
            Program.servername = cbKhoa.SelectedValue.ToString();
            Program.mlogin = txtUsername.Text;
            Program.password = txtPassword.Text;
            Program.mChinhanh = cbKhoa.SelectedIndex;
            Program.mloginDN = Program.mlogin;
            Program.passwordDN = Program.password;
            if(Program.KetNoi() == 0)
            {
                XtraMessageBox.Show("Sai tên đăng nhập hoăc mật khẩu", "Nhập sai");
                return;
            }
          
            SqlDataReader reader = Program.ExecSqlDataReader("EXEC dbo.SP_DANGNHAP '" + txtUsername.Text + "'");

            if (!reader.Read())
            {
                return;
            }
            if (reader.GetString(2) == "PGV")
            {
                Program.role = ROLE.PGV;
            }
            else if (reader.GetString(2) == "Khoa")
            {
                Program.role = ROLE.Khoa;
            }
            else if (reader.GetString(2) == "PKeToan")
            {
                Program.role = ROLE.PKeToan;
            }
            frmMain frm = new frmMain();
            frm.htMaGV.Caption =  "Mã giảng viên: " + reader.GetString(0);
            frm.htTenGV.Caption = "Tên giảng viên: " +reader.GetString(1);
            frm.htRole.Caption = "Role: "  + reader.GetString(2);
          
            frm.Show();
            this.Hide();
            frm.FormClosing += Frm_FormClosing;
        }

        private void Frm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.bds_dspm.Filter = "";
            this.Show();
            Program.isClass = false;
        }

        private void CbKhoa_SelectedIndexChanged(object sender, EventArgs e)
        {
            Program.isClass = false;
        }
    }
}
