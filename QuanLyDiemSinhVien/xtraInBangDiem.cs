﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QuanLyDiemSinhVien
{
    public partial class xtraInBangDiem : DevExpress.XtraReports.UI.XtraReport
    {
        public xtraInBangDiem(string maLop, string maMonHoc, int lanThi)
        {
            InitializeComponent();
            ds1.EnforceConstraints = false;
            this.sp_BangDiemTableAdapter1.Fill(ds1.sp_BangDiem, maLop,maMonHoc,lanThi);
        }

    }
}
