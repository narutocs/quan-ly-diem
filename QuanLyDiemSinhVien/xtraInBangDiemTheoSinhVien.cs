﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QuanLyDiemSinhVien
{
    public partial class xtraInBangDiemTheoSinhVien : DevExpress.XtraReports.UI.XtraReport
    {
        public xtraInBangDiemTheoSinhVien(string maSinhVien)
        {
            InitializeComponent();

            ds1.EnforceConstraints = false;
            this.sp_BangDiemTheoSinhVienTableAdapter.Fill(ds1.sp_BangDiemTheoSinhVien, maSinhVien);
        }

    }
}
