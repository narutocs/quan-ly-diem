﻿namespace QuanLyDiemSinhVien
{
    partial class frmNhapSinhVien
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtMaSinhVien = new DevExpress.XtraEditors.TextEdit();
            this.txtHo = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txten = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtDiaChi = new DevExpress.XtraEditors.TextEdit();
            this.txtNoiSinh = new DevExpress.XtraEditors.TextEdit();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.btnGhi = new System.Windows.Forms.Button();
            this.rbNam = new System.Windows.Forms.RadioButton();
            this.rbNu = new System.Windows.Forms.RadioButton();
            this.txtNgaySinh = new DevExpress.XtraEditors.DateEdit();
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.dS = new QuanLyDiemSinhVien.DS();
            this.BSGetClass = new System.Windows.Forms.BindingSource(this.components);
            this.getListClassTableAdapter = new QuanLyDiemSinhVien.DSTableAdapters.GetListClassTableAdapter();
            this.tableAdapterManager = new QuanLyDiemSinhVien.DSTableAdapters.TableAdapterManager();
            this.txtLop = new DevExpress.XtraEditors.TextEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.rbFalse = new System.Windows.Forms.RadioButton();
            this.rbTrue = new System.Windows.Forms.RadioButton();
            this.pnNghiHoc = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaSinhVien.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txten.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiSinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgaySinh.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgaySinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSGetClass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLop.Properties)).BeginInit();
            this.pnNghiHoc.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(18, 19);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(63, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Mã sinh viên:";
            // 
            // txtMaSinhVien
            // 
            this.txtMaSinhVien.Enabled = false;
            this.txtMaSinhVien.Location = new System.Drawing.Point(88, 12);
            this.txtMaSinhVien.Name = "txtMaSinhVien";
            this.txtMaSinhVien.Size = new System.Drawing.Size(234, 20);
            this.txtMaSinhVien.TabIndex = 1;
            // 
            // txtHo
            // 
            this.txtHo.EditValue = "";
            this.txtHo.Location = new System.Drawing.Point(88, 38);
            this.txtHo.Name = "txtHo";
            this.txtHo.Size = new System.Drawing.Size(234, 20);
            this.txtHo.TabIndex = 2;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(19, 45);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(17, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Họ:";
            // 
            // txten
            // 
            this.txten.EditValue = "";
            this.txten.Location = new System.Drawing.Point(88, 64);
            this.txten.Name = "txten";
            this.txten.Size = new System.Drawing.Size(234, 20);
            this.txten.TabIndex = 3;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(18, 71);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(22, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Tên:";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(18, 97);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(35, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "Mã lớp:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(19, 123);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(24, 13);
            this.labelControl5.TabIndex = 8;
            this.labelControl5.Text = "Phái:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(19, 149);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(51, 13);
            this.labelControl6.TabIndex = 10;
            this.labelControl6.Text = "Ngày sinh:";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.EditValue = "";
            this.txtDiaChi.Location = new System.Drawing.Point(88, 194);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(234, 20);
            this.txtDiaChi.TabIndex = 9;
            // 
            // txtNoiSinh
            // 
            this.txtNoiSinh.EditValue = "";
            this.txtNoiSinh.Location = new System.Drawing.Point(88, 168);
            this.txtNoiSinh.Name = "txtNoiSinh";
            this.txtNoiSinh.Size = new System.Drawing.Size(234, 20);
            this.txtNoiSinh.TabIndex = 8;
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(19, 227);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(39, 13);
            this.labelControl8.TabIndex = 14;
            this.labelControl8.Text = "Ghi chú:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(19, 201);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(36, 13);
            this.labelControl9.TabIndex = 16;
            this.labelControl9.Text = "Địa chỉ:";
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(19, 175);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(41, 13);
            this.labelControl10.TabIndex = 18;
            this.labelControl10.Text = "Nơi sinh:";
            // 
            // btnGhi
            // 
            this.btnGhi.Location = new System.Drawing.Point(148, 304);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(75, 23);
            this.btnGhi.TabIndex = 20;
            this.btnGhi.Text = "GHI";
            this.btnGhi.UseVisualStyleBackColor = true;
            this.btnGhi.Click += new System.EventHandler(this.BtnGhi_Click);
            // 
            // rbNam
            // 
            this.rbNam.AutoSize = true;
            this.rbNam.Checked = true;
            this.rbNam.Location = new System.Drawing.Point(126, 117);
            this.rbNam.Name = "rbNam";
            this.rbNam.Size = new System.Drawing.Size(46, 17);
            this.rbNam.TabIndex = 5;
            this.rbNam.TabStop = true;
            this.rbNam.Text = "Nam";
            this.rbNam.UseVisualStyleBackColor = true;
            // 
            // rbNu
            // 
            this.rbNu.AutoSize = true;
            this.rbNu.Location = new System.Drawing.Point(223, 117);
            this.rbNu.Name = "rbNu";
            this.rbNu.Size = new System.Drawing.Size(39, 17);
            this.rbNu.TabIndex = 6;
            this.rbNu.Text = "Nữ";
            this.rbNu.UseVisualStyleBackColor = true;
            // 
            // txtNgaySinh
            // 
            this.txtNgaySinh.EditValue = null;
            this.txtNgaySinh.Location = new System.Drawing.Point(88, 140);
            this.txtNgaySinh.Name = "txtNgaySinh";
            this.txtNgaySinh.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNgaySinh.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtNgaySinh.Properties.DisplayFormat.FormatString = "MM/dd/yyyy";
            this.txtNgaySinh.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgaySinh.Properties.EditFormat.FormatString = "dd/MM/yyyy";
            this.txtNgaySinh.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtNgaySinh.Size = new System.Drawing.Size(234, 20);
            this.txtNgaySinh.TabIndex = 7;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(88, 220);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(234, 44);
            this.txtGhiChu.TabIndex = 10;
            this.txtGhiChu.Text = "   ";
            // 
            // dS
            // 
            this.dS.DataSetName = "DS";
            this.dS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // BSGetClass
            // 
            this.BSGetClass.DataMember = "GetListClass";
            this.BSGetClass.DataSource = this.dS;
            // 
            // getListClassTableAdapter
            // 
            this.getListClassTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.DIEMTableAdapter = null;
            this.tableAdapterManager.GIANGVIENTableAdapter = null;
            this.tableAdapterManager.KHOATableAdapter = null;
            this.tableAdapterManager.LOPTableAdapter = null;
            this.tableAdapterManager.MONHOCTableAdapter = null;
            this.tableAdapterManager.SINHVIENTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = QuanLyDiemSinhVien.DSTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // txtLop
            // 
            this.txtLop.Enabled = false;
            this.txtLop.Location = new System.Drawing.Point(88, 91);
            this.txtLop.Name = "txtLop";
            this.txtLop.Size = new System.Drawing.Size(234, 20);
            this.txtLop.TabIndex = 4;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(7, 7);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(45, 13);
            this.labelControl7.TabIndex = 27;
            this.labelControl7.Text = "Nghỉ học:";
            // 
            // rbFalse
            // 
            this.rbFalse.AutoSize = true;
            this.rbFalse.Checked = true;
            this.rbFalse.Location = new System.Drawing.Point(211, 7);
            this.rbFalse.Name = "rbFalse";
            this.rbFalse.Size = new System.Drawing.Size(50, 17);
            this.rbFalse.TabIndex = 12;
            this.rbFalse.TabStop = true;
            this.rbFalse.Text = "False";
            this.rbFalse.UseVisualStyleBackColor = true;
            // 
            // rbTrue
            // 
            this.rbTrue.AutoSize = true;
            this.rbTrue.Location = new System.Drawing.Point(114, 7);
            this.rbTrue.Name = "rbTrue";
            this.rbTrue.Size = new System.Drawing.Size(47, 17);
            this.rbTrue.TabIndex = 11;
            this.rbTrue.Text = "True";
            this.rbTrue.UseVisualStyleBackColor = true;
            // 
            // pnNghiHoc
            // 
            this.pnNghiHoc.Controls.Add(this.rbTrue);
            this.pnNghiHoc.Controls.Add(this.rbFalse);
            this.pnNghiHoc.Controls.Add(this.labelControl7);
            this.pnNghiHoc.Location = new System.Drawing.Point(12, 270);
            this.pnNghiHoc.Name = "pnNghiHoc";
            this.pnNghiHoc.Size = new System.Drawing.Size(321, 28);
            this.pnNghiHoc.TabIndex = 30;
            // 
            // frmNhapSinhVien
            // 
            this.AcceptButton = this.btnGhi;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 339);
            this.Controls.Add(this.pnNghiHoc);
            this.Controls.Add(this.txtLop);
            this.Controls.Add(this.txtGhiChu);
            this.Controls.Add(this.txtNgaySinh);
            this.Controls.Add(this.rbNu);
            this.Controls.Add(this.rbNam);
            this.Controls.Add(this.btnGhi);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.txtNoiSinh);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.txtDiaChi);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.txten);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.txtHo);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.txtMaSinhVien);
            this.Controls.Add(this.labelControl1);
            this.Name = "frmNhapSinhVien";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin sinh viên";
            this.Load += new System.EventHandler(this.FrmNhapSinhVien_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtMaSinhVien.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtHo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txten.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiaChi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoiSinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgaySinh.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgaySinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BSGetClass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLop.Properties)).EndInit();
            this.pnNghiHoc.ResumeLayout(false);
            this.pnNghiHoc.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private System.Windows.Forms.Button btnGhi;
        public DevExpress.XtraEditors.TextEdit txtMaSinhVien;
        public DevExpress.XtraEditors.TextEdit txtHo;
        public DevExpress.XtraEditors.TextEdit txten;
        public DevExpress.XtraEditors.TextEdit txtDiaChi;
        public DevExpress.XtraEditors.TextEdit txtNoiSinh;
        private DS dS;
        private System.Windows.Forms.BindingSource BSGetClass;
        private DSTableAdapters.GetListClassTableAdapter getListClassTableAdapter;
        private DSTableAdapters.TableAdapterManager tableAdapterManager;
        public System.Windows.Forms.RadioButton rbNam;
        public System.Windows.Forms.RadioButton rbNu;
        public DevExpress.XtraEditors.DateEdit txtNgaySinh;
        public System.Windows.Forms.TextBox txtGhiChu;
        public DevExpress.XtraEditors.TextEdit txtLop;
        public System.Windows.Forms.RadioButton rbTrue;
        public System.Windows.Forms.RadioButton rbFalse;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        public System.Windows.Forms.Panel pnNghiHoc;
    }
}