﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;

namespace QuanLyDiemSinhVien
{
    public partial class frmMonHoc : DevExpress.XtraEditors.XtraForm
    {
        public frmMonHoc()
        {
            InitializeComponent();
        }
        public Stack<LogAction> logData = new Stack<LogAction>();
        public string maMonHocTemp;
        public string tenMonHocTemp;
        private void MONHOCBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.BSMONHOC.EndEdit();
            this.tableAdapterManager.UpdateAll(this.dS);

        }

        private void FrmMonHoc_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dS.DIEM' table. You can move, or remove it, as needed.
           
            this.mONHOCTableAdapter.Fill(this.dS.MONHOC);
            this.dIEMTableAdapter.Fill(this.dS.DIEM);
        }

        private void BtnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            txtMaMonHoc.Enabled = txtTenMonHoc.Enabled = true;
            btnThem.Enabled = false;
            btnXoa.Enabled = false;
            btnUndo.Enabled = false;
            btnEdit.Enabled = false;
            btnAdd.Enabled = true;
            BSMONHOC.AddNew();
        }
        public int xoaSubject(string ma)
        {
            string query = "sp_DeleteSubject";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ma", ma);
            return command.ExecuteNonQuery();
        }
        private void BtnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(BSDIEM.Count > 0)
            {
                MessageBox.Show("Không thể xóa môn học này");
            }
            else
            {
                DialogResult ret = MessageBox.Show("Bạn có chắc muốn xóa?", "Cảnh báo xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (ret == DialogResult.Yes)
                {
                    string ma = txtMaMonHoc.Text;
                    btnUndo.Enabled = true;
                    Dictionary<string, object> dataDeleted = new Dictionary<string, object>();
                    dataDeleted.Add("mamonhoc", txtMaMonHoc.Text);
                    dataDeleted.Add("tenmonhoc", txtTenMonHoc.Text);
                    logData.Push(new LogAction(TYPE.DELETE, dataDeleted)); // add data cũ
                    int result = xoaSubject(ma);
                    if (result == -1)
                    {
                        MessageBox.Show("Xóa thất bại");
                    }
                    else
                    {
                        BSMONHOC.EndEdit();
                        BSMONHOC.RemoveCurrent();
                    }
                }
            }
           
        }

        private void BtnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            maMonHocTemp = txtMaMonHoc.Text;
            tenMonHocTemp = txtTenMonHoc.Text;
            txtTenMonHoc.Enabled = true;
            txtTenMonHoc.Focus();
            btnUndo.Enabled = false;
            btnEdit.Enabled = false;
            btnThem.Enabled = false;
            btnXoa.Enabled = false;
            btnAdd.Enabled = true;
        }
        public int themSubject(string ma, string ten)
        {
            string query = "sp_InsertSubject";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ma", ma);
            command.Parameters.AddWithValue("@ten", ten);
            return command.ExecuteNonQuery(); 
        }

        public int suaSubject(string ma, string ten)
        {
            string query = "sp_UpdateSubject";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@ma", ma);
            command.Parameters.AddWithValue("@ten", ten);
            return command.ExecuteNonQuery();
        }
        private void BtnAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string ma = txtMaMonHoc.Text;
            string ten = txtTenMonHoc.Text;
            if (txtMaMonHoc.Enabled)
            {
                int result = themSubject(ma, ten);
                if(result == -1)
                {
                    MessageBox.Show("Insert thất bại");
                }
                else
                {
                    btnUndo.Enabled = true;
                    Dictionary<string, object> dataDeleted = new Dictionary<string, object>();
                    dataDeleted.Add("mamonhoc", txtMaMonHoc.Text);
                    dataDeleted.Add("tenmonhoc", txtTenMonHoc.Text);
                    logData.Push(new LogAction(TYPE.ADD, dataDeleted)); // add data cũ
                    BSMONHOC.EndEdit();
                    BSMONHOC.ResetCurrentItem();
                    txtMaMonHoc.Enabled = txtTenMonHoc.Enabled = false;
                    btnThem.Enabled = true;
                    btnAdd.Enabled = false;
                    btnXoa.Enabled = true;
                    btnEdit.Enabled = true;
                }
            }
            else
            {
                int result = suaSubject(ma, ten);
                if (result == -1)
                {
                    MessageBox.Show("Update thất bại");
                }
                else
                {
                    btnUndo.Enabled = true;
                    Dictionary<string, object> dataDeleted = new Dictionary<string, object>();
                    dataDeleted.Add("mamonhoc", maMonHocTemp);
                    dataDeleted.Add("tenmonhoc", tenMonHocTemp);
                    logData.Push(new LogAction(TYPE.UPDATE, dataDeleted)); // add data cũ
                    BSMONHOC.EndEdit();
                    BSMONHOC.ResetCurrentItem();
                    txtMaMonHoc.Enabled = txtTenMonHoc.Enabled = false;
                    btnThem.Enabled = true;
                    btnAdd.Enabled = false;
                    btnXoa.Enabled = true;
                    btnEdit.Enabled = true;
                }
            }
        }

        private void BtnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (logData.Count > 0)
            {
                LogAction action = logData.Pop();
                if (logData.Count == 0)
                {
                    btnUndo.Enabled = false;
                }
                Dictionary<string, object> row = action.data;
                string mamonhoc = row["mamonhoc"].ToString();
                string tenmonhoc = row["tenmonhoc"].ToString();
                switch (action.type)
                {
                    case TYPE.ADD:
                        xoaSubject(mamonhoc);
                        BSMONHOC.EndEdit();
                        this.mONHOCTableAdapter.Fill(this.dS.MONHOC);
                        break;
                    case TYPE.DELETE:
                        themSubject(mamonhoc, tenmonhoc);
                        BSMONHOC.EndEdit();
                        this.mONHOCTableAdapter.Fill(this.dS.MONHOC);
                        break;
                    case TYPE.UPDATE:
                        suaSubject(mamonhoc, tenmonhoc);
                        BSMONHOC.EndEdit();
                        this.mONHOCTableAdapter.Fill(this.dS.MONHOC);
                        break;
                }
            }
        }

        private void BtnRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.mONHOCTableAdapter.Fill(this.dS.MONHOC);
        }

        private void Btnthoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            txtMaMonHoc.Enabled = txtTenMonHoc.Enabled = false;
            btnThem.Enabled = true;
            btnAdd.Enabled = false;
            btnXoa.Enabled = true;
            btnEdit.Enabled = true;
            this.mONHOCTableAdapter.Fill(this.dS.MONHOC);
        }
    }
}