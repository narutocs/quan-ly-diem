﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Repository;
using System.Globalization;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Columns;

namespace QuanLyDiemSinhVien
{
    public partial class frmHocPhi : DevExpress.XtraEditors.XtraForm
    {
        public frmHocPhi()
        {
            InitializeComponent();

        }

        public bool isChange = true;
        public string currentHocPhi = "";
        public string currentTienDaDong = "";
        public string maSV;
        private void BtnXacNhan_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtMaSinhVien.Text))
            {
                MessageBox.Show("Mã sinh viên không được để trống");
                return;
            }

            maSV = txtMaSinhVien.Text;
            string query = "sp_GetInfoStudent";
            SqlCommand command = new SqlCommand(query, Program.conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@maSV", maSV);
            DataTable table = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(table);
            if(table.Rows.Count > 0)
            {
                object[] data = table.Rows[0].ItemArray;
                txtTen.Text = data[0].ToString();
                txtMaLop.Text = data[1].ToString();
                txtTenLop.Text = data[2].ToString();
                query = "sp_GetHocPhi";
                command = new SqlCommand(query, Program.conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@maSV", maSV);
                table = new DataTable();
                adapter = new SqlDataAdapter(command);
                adapter.Fill(table);
                
                gridControl1.DataSource = table;
                RepositoryItemComboBox _riEditor = new RepositoryItemComboBox();
                RepositoryItemComboBox _riEditorHocKy = new RepositoryItemComboBox();

                _riEditor.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
                _riEditorHocKy.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
                int currentYear = DateTime.Now.Year;
                int index = -4;
                string[] tempYear = new string[11];
                for(int i = 0; i <= 10; i++)
                {
                    tempYear[i] = (currentYear + index) + " - " + (currentYear + index+1);
                    index++;
                }
                _riEditor.Items.AddRange(tempYear);
                gridControl1.RepositoryItems.Add(_riEditor);
                gridView1.Columns[0].ColumnEdit = _riEditor;

                _riEditorHocKy.Items.AddRange(new string[] { "1","2","3"});
                gridControl1.RepositoryItems.Add(_riEditorHocKy);
                gridView1.Columns[1].ColumnEdit = _riEditorHocKy;
                gridView1.AddNewRow();
                gridView1.AddNewRow();
                gridView1.AddNewRow();
                gridView1.AddNewRow();
                gridView1.AddNewRow();

            }
            else
            {
                txtTen.Text = "";
                txtMaLop.Text = "";
                txtTenLop.Text = "";
                gridControl1.DataSource = table;
            }
            
        }

        private void BtnGhi_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gridView1.DataRowCount; i++)
            {
                string nienKhoa = gridView1.GetRowCellValue(i, "NIENKHOA").ToString().Trim();
                string hocPhi = gridView1.GetRowCellValue(i, "HOCPHI").ToString().Trim();
                string hocKy = gridView1.GetRowCellValue(i, "HOCKY").ToString().Trim();
                string soTienDaDong = gridView1.GetRowCellValue(i, "SOTIENDADONG").ToString().Trim();

                if(string.IsNullOrEmpty(soTienDaDong) || string.IsNullOrEmpty(hocPhi))
                {
                    continue;
                }
                
                    string query = "sp_InsertHocPhi";
                    SqlCommand command = new SqlCommand(query, Program.conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@nienKhoa", nienKhoa);
                    command.Parameters.AddWithValue("@maSV", maSV);
                    command.Parameters.AddWithValue("@hocKy", hocKy);
                    command.Parameters.AddWithValue("@hocPhi", hocPhi);
                    command.Parameters.AddWithValue("@soTienDaDong", soTienDaDong);
                    command.ExecuteNonQuery();
                
            }
            MessageBox.Show("Cập nhật học phí cho sinh viên thành công");
        }

        private void GridView1_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.Caption == "Học phí")
            {
                if (isChange)
                {

                    string tempHocPhi = e.Value.ToString();
                    if (!int.TryParse(tempHocPhi, out int hocPhi))
                    {
                        MessageBox.Show("Tiền học phí không hợp lệ");
                    }
                    else if (hocPhi < 0)
                    {
                        MessageBox.Show("Số tiền đã đóng không hợp lệ");
                        isChange = false;
                    }
                }
                else
                {
                    isChange = true;
                }
            }
            else if(e.Column.Caption == "Số tiền đã đóng")
            {
                if (isChange)
                {
                    string temp = e.Value.ToString();
                    
                    if (!int.TryParse(temp, out int soTienDaDong))
                    {
                        MessageBox.Show("Số tiền đã đóng không hợp lệ");
                    }
                    else if (soTienDaDong < 0 )
                    {
                        MessageBox.Show("Số tiền đã đóng không hợp lệ");
                        isChange = false;
                    }
                }
                else
                {
                    isChange = true;
                }
            }
        }

     

        private void GridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;
            string val = e.Value.ToString();
            int result;
            if(view.FocusedColumn.FieldName == "HOCPHI")
            {
                if (string.IsNullOrEmpty(val))
                {
                    e.Valid = false;
                    e.ErrorText = "Học phí không được để trống";
                }
                else if(int.TryParse(val,out result))
                {
                    if(result < 0)
                    {
                        e.Valid = false;
                        e.ErrorText = "Học phí không hợp lệ";
                    }
                }
                else
                {
                    e.Valid = false;
                    e.ErrorText = "Học phí không hợp lệ";
                }
            }
            else if(view.FocusedColumn.FieldName == "SOTIENDADONG")
            {
                if (string.IsNullOrEmpty(val))
                {
                    e.Valid = false;
                    e.ErrorText = "Tiền đã đóng không được để trống";
                }
                else if (int.TryParse(val, out result))
                {
                    if (result < 0)
                    {
                        e.Valid = false;
                        e.ErrorText = "Tiền đã đóng không hợp lệ";
                    }
                }
                else
                {
                    e.Valid = false;
                    e.ErrorText = "Tiền đã đóng không hợp lệ";
                }
            }
        }

        private void GridView1_CellValueChanged_1(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {

        }
    }
}